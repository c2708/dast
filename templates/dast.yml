#     DAST Api Scan GitLab Component
#     Copyright (C) 2024 Christian Maurer
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
spec:
  inputs:
    stage:
      default: test
    target:
      description: target url
      default: https://www.zaproxy.org/
    format:
      description: format openapi, soap, or graphql
      options: ["openapi", "soap", "graphql"]
      default: openapi
    rules_file_name:
      description: relative path for the ZAP configuration file
      default: .
    api_image:
      description: ZAP API Scan image name
      default: softwaresecurityproject/zap-stable
    cmd_options:
      description: additional command line options
      default: ""
    zap_working_dir:
      description: zap working directory `/zap/wrk`
      default: $CI_PROJECT_DIR/zap/wrk
    zap_report_dir:
      description: zap reports directory `/zap/wrk/reports`
      default: $CI_PROJECT_DIR/zap/wrk/reports
    json_report_name:
      description: file name json report
      default: zap_api_report.json
---
workflow:
  rules:
    - if: '$DAST_DISABLED == "true"'
      when: never
    - when: always

variables:
  ZAP_API_IMAGE: $[[ inputs.api_image ]]
  ZAP_WORKING_DIR: $[[ inputs.zap_working_dir ]]
  ZAP_REPORTS_DIR: $[[ inputs.zap_report_dir ]]
  ZAP_TARGET: $[[ inputs.target ]]
  ZAP_FORMAT: $[[ inputs.format ]]
  ZAP_JSON_REPORT_NAME: $[[ inputs.json_report_name ]]
  ZAP_CMD_OPTIONS: $[[ inputs.cmd_options ]]

dast-api-scan:
  image: docker:20.10.12
  stage: $[[ inputs.stage ]]
  services:
    - name: "docker:20.10.12-dind"
      command: ["--tls=false", "--host=tcp://0.0.0.0:2375"]
  script:
    - mkdir -p $ZAP_REPORTS_DIR $ZAP_WORKING_DIR
    - echo report file "$ZAP_REPORTS_DIR/$ZAP_JSON_REPORT_NAME"
    - touch "$ZAP_REPORTS_DIR/$ZAP_JSON_REPORT_NAME"
    - chmod a+w "$ZAP_REPORTS_DIR/$ZAP_JSON_REPORT_NAME"
    - docker pull --quiet $ZAP_API_IMAGE
    - docker run --network="host" --volume $ZAP_REPORTS_DIR:/zap/wrk/reports/:rw  --volume $ZAP_WORKING_DIR:/zap/wrk/:rw --volume /var/run/docker.sock:/var/run/docker.sock $ZAP_API_IMAGE zap-api-scan.py -t ${ZAP_TARGET} -f ${ZAP_FORMAT} -J "/zap/wrk/reports/$ZAP_JSON_REPORT_NAME" ${ZAP_CMD_OPTIONS}
  needs: []
  artifacts:
    reports:
      dast: "$ZAP_REPORTS_DIR/$ZAP_JSON_REPORT_NAME"
