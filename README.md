# DAST - Dynamic Application Security Testing

This component uses [OWASP ZAP](https://www.zaproxy.org/) for DAST.

## DAST [API Scan](https://www.zaproxy.org/docs/docker/api-scan/)

Performs a scan against a REST Api defined by OpenApi, SOAP, or GraphQL against a URL.

## Usage

Use this component to perform a DAST scan a REST Api and get a report for vulnerabilities.

```yaml
include:
  - component: gitlab.com/c2708/dast
    inputs:
      stage: test
      format: openapi
```

### Overriding job attributes

To override job attributes (i.e. `allow_failure`), add an element with the name `dast-api-scan`, and update the specific attribute.

```yaml
include:
  - component: gitlab.com/c2708/dast
    inputs:
      stage: test
      format: openapi

dast-api-scan:
  allow_failure: true
```

## Inputs

| Input | Default Value | Description |
| ----- | ------------- | ----------- |
| stage | `test`        |  the stage where you want the job added |
| target | `https://www.zaproxy.org/` | target url to be tested |
| format | `openapi`    | Api Definition format.  One of `openapi`, `soap`, or `graphql` |
| rules_file_name | `.` | relative path for the ZAP configuration file |
| api_image | `softwaresecurityproject/zap-stable` | ZAP API Scan image name |
| cmd_options | "" | additional zap command line options. see [ZAP - API Scan](https://www.zaproxy.org/docs/docker/api-scan/) for options |
| zap_working_dir | `$CI_PROJECT_DIR/zap/wrk` | zap working directory.  mapped to `/zap/wrk`. |
| zap_report_dir | `$CI_PROJECT_DIR/zap/wrk/reports` | directory where scan results are saved |
| json_report_name | `zap_api_report.json` | name of the json report file |

## Variables

| CI/CD Variable | Description |
| -------------- | ----------- |
| `DAST_DISABLED` | Set to "true" to disable dast scanning |
